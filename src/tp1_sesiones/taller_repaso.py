## 1.Imprimir "Hola mundo".
print("Hola mundo")

## 2.Variables y asignacion.

variable = 10
print(variable)
variable += 4
print (variable)

## 3.Tipos de datos básicos: int, float, string.

numero_entero=int(20)
numero_flotante=float(4.5)
String=str('Taller repaso')

print(numero_entero)
print(numero_flotante)
print(String)

## 4.Operaciones aritméticas básicas.

#Suma
suma=int(2+5)
print(suma)

#Resta
resta=int(10-8)
print(resta)

#Multiplicar
mult=int(5*5)
print(mult)

#Dividir
div=int(5/5)
print(div)

## 5.Conversión entre tipos de datos.

variableX = 26
print("La variable al principio es: {variableX}")
print("Varible tipo int: {int(variableX)}")
print("Varible tipo float: {float(variableX)}")
print("Varible tipo String: {str(variableX)}")
print("Varible tipo Caracter: {chr(variableX)}")

## 6.Solicitar entrada del usuario.

numero_entero=input("Ingrese el numero: " )
print("EL numero es: " + numero_entero)

## 7.Condicional if.



## 8.Condicional if-else

a=int
b=int 

a=input("Ingrese un numero: " )
b=input("Ingrese un numero: " )

if (a<b) :

    print("El numero " + a + " Es menor al numero "+ b)
else:

 print("El numero " + a + " Es mayor al numero "+ b)

 ## 9.Condicional if-elif-else.

a=int
b=int 

a=input("Ingrese un numero: " )
b=input("Ingrese un numero: " )

if (a<b) :

    print("El numero " + a + " Es menor al numero "+ b)
elif(a>b):

 print("El numero " + a + " Es mayor al numero "+ b)
else:
   print("El numero " + a + " Es igual al numero "+ b)

##10. Bucle for.

nums = [4, 78, 9, 84]
for n in nums:
    print(n)
    print("Ciclo para")
## 11.Bucle while.

i=1
while i<6:
   print(i)
   i +=1
print("Ciclo mientras")

## 12. Uso de break y continue.

#Break

numero = 0

for numero in range(10):
    if numero == 5:
        break 

    print('Numero es ' + str(numero))

print('Fuera de circuito')

#Continue

numero = 0

for numero in range(10):
    if numero == 5:
        continue    # continue here

    print('Number is ' + str(numero))

print('Fuera de circuito')

## 13. Listas y sus operaciones básicas.

list = [0,1,2,3,4,5,6,7,8,9,10,11]
list2 = [13,14,15,16,17,18,19,20] 
list.append(12)
print (list)
list.extend(list2)
print (list)
list.insert (6,5.5)
print(list)
list[6] = 33
print (list)
list[6] = 33/3
print(list)
del list[6]
print(list)
list.remove(11)
print (list)
list.reverse()
print(list)
list.sort()
print(list)
list.reverse()
list2 = sorted(list)
print(list2)

## 14.Tuplas y su inmutabilidad.

tupla = (1, 2, 3)
print(type(tupla)) 
print(tupla)

## 15.Conjuntos y operaciones.

cojuntos={1,2,3,4,5,6}
cojuntos.add(7)
print(cojuntos)
cojuntos.remove(1)
print(cojuntos)

## 16.Diccionarios y operaciones clave-valor.

diccionario={
   
   "Nombre":"Juan",
   "Edad": 25,
   "Fecha":"23/12/2023",
}

diccionario["Apellido"]="Marin"
del diccionario["Fecha"]
print(diccionario)

## 17.Leer un archivo de texto.

with open("./src/tp1_sesiones/static/txt/documento.txt","r") as archivo:
    print(archivo.read())

## 18 Escribir en un archivo de texto.

with open("./src/tp1_sesiones/static/txt/documnto.txt","w") as archivo:
    archivo.write("Taller repaso")

## 19. Modos de apertura: r, w, a, rb, wb.

with open('archivo.txt', 'r') as archivo:
    archivo.read()
    
    with open('archivo.txt', 'w') as archivo:
        archivo.write ("texto")
        
        with open('archivo.txt', 'a') as archivo:
            archivo.write("texto")
              
        with open('archivo.bin', 'rb') as archivo_binario:
         archivo.read()
         
         with open('archivo.bin', 'wb') as archivo_binario:
              archivo.write (" ")

## 20. Trabajar con archivos JSON.

import json 
datos={
    "Nombre":"Jose",
    "Edad":"20",
    "Documento":"12345"    
}
with open("./src/tp1_sesiones/static/json/taller1.json","w") as js:
    json.dump(datos, js) #dump es para guardar los datos de un json

with open("./src/tp1_sesiones/static/json/taller1.json","r") as js:
    Archivo=json.load(js) #load es para cargar un archivo.json
    print(Archivo)

#Crear un dataframe
import pandas as pd

informacion = {
    "producto": ["Moto", "Carro", "Biciclete", "Patineta"],
    "precio": [100, 305, 143, 275],
    "color": ["Rojo", "Azul", "Morado", "Verte"]
  
}
df = pd.DataFrame(informacion)

#21 Leer un archivo CSV.

df = pd.read_csv('./src/tp1_sesiones/static/csv/documento.csv')
print(df)

# 22. Filtrar datos en un DataFrame.

precios_mayores = df[df['precio'] > 200]
print(precios_mayores)

#23 Operaciones básicas: sum(), mean(), max(), min()

suma_precios = df.sum()
print(suma_precios)

promedio_total = df.mean()
print(promedio_total)

maxima_cantidad = df.max()
print(maxima_cantidad)

minimo_cantidad = df.min()
print(minimo_cantidad)

#24 Uso de iloc y loc

fila = df.iloc[0] 
filas = df.iloc[0:2]  

fila = df.loc['producto']

#25 Agrupar datos con groupby

grouped_df = df.groupby('color')
print(grouped_df.get_group('Morado'))

#26 Unir DataFrames con merge y concat

informacion2 = {
    "producto": ["Bmw", "Mazda", "Ford", "Hyundai"],
    "precio": [129, 415, 531, 231],
    "color": ["Blanco", "Gris", "Rojo", "Negro"]
}
df2 = pd.DataFrame(informacion2)

pd.concat([df, df2], axis=1)
df.merge(df2, on="precio", how="left")

#27 Manipular series temporales

import datetime
fechas = [datetime.date(2023, 26, 8), datetime.date(2023, 27, 9), datetime.date(2023, 28, 10)]
valores = [100, 150, 200]

serie_temporal = pd.Series(valores, index=fechas)

#28 Exportar un DataFrame a CSV
df.to_csv("src/tp1_sesiones/static/csv/documento.csv")

#29 Convertir un DataFrame a JSON
df.to_json('archivo.json')