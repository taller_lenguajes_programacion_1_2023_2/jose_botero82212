from django.db import models
from webinicio.models import AuditoriaFecha
# Create your models here.




class Producto(AuditoriaFecha):
    nombre_prod = models.CharField(max_length=255)
    descripcion_prod=models.CharField(max_length=255)
    cantidad_prod=models.IntegerField()
    estado = models.BooleanField(default=True)
    valor = models.FloatField()
    
    def __str__(self) :
        return "Producto:  {} $: {} #: {}".format(self.nombre_prod,self.valor,self.cantidad_prod)