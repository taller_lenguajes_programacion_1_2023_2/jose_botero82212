from django import forms
from .models import Usuario,Persona
from .models import UsuarioRegistrado


class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.EmailInput())
    clave = forms.CharField(required=True,widget=forms.PasswordInput())
    
    class Meta:
        model = Usuario
class RegistroForm(forms.ModelForm):
    class Meta:
        model = UsuarioRegistrado
        fields = ['imagen', 'nombre', 'apellido', 'cedula', 'correo', 'usuario', 'contraseña', 'habilidades', 'herramientas']
        widgets = {
            'contraseña': forms.PasswordInput(),
        }
