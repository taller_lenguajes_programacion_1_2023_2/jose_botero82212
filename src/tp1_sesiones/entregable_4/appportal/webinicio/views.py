from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import LoginForm
from .models import Usuario, Persona

# Create your views here.

def inicio(request):
    # return HttpResponse("estoy en la pagina de inicio")
    return render(request,'index.html')

def login(request):
    mensaje={"estado": "", "mensaje":"Bienvenido"    }
    usuariodb = None
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        usuario = request.POST.get('usuario','')
        clave = request.POST.get('clave','')
        print(usuario,clave)
        usuariodb = Usuario.objects.filter(usuario=usuario,clave=clave)
        if len(usuariodb)>0:
            return redirect("portal")
        else:
            mensaje['form_login']=form
            return render(request,'login.html',mensaje)
    # return HttpResponse("estoy en la pagina de inicio")
    else:
        form = LoginForm(data=request.GET)
        mensaje['form_login']=form
    return render(request,'login.html',mensaje)

def registro(request):
    # return HttpResponse("estoy en la pagina de inicio")
    return render(request,'registro.html')
