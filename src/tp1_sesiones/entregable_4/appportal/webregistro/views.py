from django.shortcuts import render, redirect
from django.http import HttpResponseServerError
from .models import Usuario

def registro_usuario(request):
    if request.method == 'POST':
        try:
            imagen = request.FILES['imagen']
            nombre = request.POST['nombre']
            apellido = request.POST['apellido']
            cedula = request.POST['cedula']
            correo = request.POST['correo']
            usuario = request.POST['usuario']
            contraseña = request.POST['contraseña']
            habilidades = request.POST['habilidades']
            herramientas = request.POST['herramientas']

            nuevo_usuario = Usuario(
                imagen=imagen,
                nombre=nombre,
                apellido=apellido,
                cedula=cedula,
                correo=correo,
                usuario=usuario,
                contraseña=contraseña,
                habilidades=habilidades,
                herramientas=herramientas
            )
            nuevo_usuario.save()

        
            return redirect('login')  

        except Exception as e:
            return HttpResponseServerError(f"Error al guardar el usuario: {e}")

    return render(request, 'registro.html')
