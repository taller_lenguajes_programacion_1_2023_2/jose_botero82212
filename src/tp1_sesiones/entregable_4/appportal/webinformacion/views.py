from django.shortcuts import render
from .models import Usuario

def mostrar_usuarios(request):
    usuarios = Usuario.objects.all()
    return render(request, 'mostrar_usuarios.html', {'usuarios': usuarios})
