# models.py
from django.db import models

class Usuario(models.Model):
    imagen = models.ImageField(upload_to='imagenes/', null=True, blank=True)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    cedula = models.CharField(max_length=20)
    correo = models.EmailField()
    usuario = models.CharField(max_length=50)
    contraseña = models.CharField(max_length=50)
    habilidades = models.TextField()
    herramientas = models.TextField()
