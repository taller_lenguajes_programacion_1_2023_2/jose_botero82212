
from django.urls import path
from . import views

urlpatterns = [
    path('usuarios/', views.mostrar_usuarios, name='mostrar_usuarios'),
]
