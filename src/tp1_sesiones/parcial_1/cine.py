from conexion_parcial_1 import Conexion_parcial_1

class Cine(Conexion_parcial_1):
    def __init__(self,id_cine,nombre,direccion,capacidad,numero_sala,telefono,correo) -> None:
        
        self.__id_cine=id_cine
        self.__nombre=nombre
        self.__direccion=direccion
        self.__capacidad=capacidad
        self.__numero_sala=numero_sala
        self.__telefono=telefono
        self.__correo=correo
        super().__init__()
        self.__crear_Cine()


    def __crear_Cine(self):
        nom_clase = "espectador"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_cine"):
            print("Tabla {} creada !!!".format(nom_clase))

    @property
    def _id_cine(self):
        return self.__id_cine

    @_id_cine.setter
    def _id_cine(self, value):
        self.__id_cine = value

    @property
    def _nombre(self):
        return self.__nombre

    @_nombre.setter
    def _nombre(self, value):
        self.__nombre = value

    @property
    def _direccion(self):
        return self.__direccion

    @_direccion.setter
    def _direccion(self, value):
        self.__direccion = value

    @property
    def _capacidad(self):
        return self.__capacidad

    @_capacidad.setter
    def _capacidad(self, value):
        self.__capacidad = value

    @property
    def _numero_sala(self):
        return self.__numero_sala

    @_numero_sala.setter
    def _numero_sala(self, value):
        self.__numero_sala = value

    @property
    def _telefono(self):
        return self.__telefono

    @_telefono.setter
    def _telefono(self, value):
        self.__telefono = value

    @property
    def _correo(self):
        return self.__correo

    @_correo.setter
    def _correo(self, value):
        self.__correo = value
    @property
    def id_cine(self):
        return self.__id_cine

    @id_cine.setter
    def id_cine(self, value):
        self.__id_cine = value

    @property
    def nombre(self):
        return self.__nombre

    @nombre.setter
    def nombre(self, value):
        self.__nombre = value

    @property
    def direccion(self):
        return self.__direccion

    @direccion.setter
    def direccion(self, value):
        self.__direccion = value

    @property
    def capacidad(self):
        return self.__capacidad

    @capacidad.setter
    def capacidad(self, value):
        self.__capacidad = value

    @property
    def numero_sala(self):
        return self.__numero_sala

    @numero_sala.setter
    def numero_sala(self, value):
        self.__numero_sala = value

    @property
    def telefono(self):
        return self.__telefono

    @telefono.setter
    def telefono(self, value):
        self.__telefono = value

    @property
    def correo(self):
        return self.__correo

    @correo.setter
    def correo(self, value):
        self.__correo = value

    def create_cine(self):
        
        self.insert_data(
            table_name="cine",
            data={
                "id_cine": self.id_cine,
                "nombre": self.nombre,
                "direccion": self.direccion,
                "capacidad": self.capacidad,
                "numero_sala": self.numero_sala,
                "telefono": self.telefono,
                "correo": self.correo
            }
        )

    @staticmethod
    def read_cine(id_cine):
       
        cine_data = Conexion_parcial_1.get_data(
            table_name="cine",
            where_conditions={"id_cine": id_cine}
        )

        if cine_data:
            data_dict = cine_data[0]  
            return Cine(
                id_cine=data_dict["id_cine"],
                nombre=data_dict["nombre"],
                direccion=data_dict["direccion"],
                capacidad=data_dict["capacidad"],
                numero_sala=data_dict["numero_sala"],
                telefono=data_dict["telefono"],
                correo=data_dict["correo"]
            )
        else:
            return None

    def update_cine(self):
        
        self.update_data(
            table_name="cine",
            data={
                "nombre": self.nombre,
                "direccion": self.direccion,
                "capacidad": self.capacidad,
                "numero_sala": self.numero_sala,
                "telefono": self.telefono,
                "correo": self.correo
            },
            where_conditions={"id_cine": self.id_cine}
        )

    def delete_cine(self):
        
        self.delete_data(
            table_name="cine",
            where_conditions={"id_cine": self.id_cine}
        )    


    def __str__(self):
     return f"{self.__id_cine}{self.__nombre} {self.__direccion} {self.__capacidad} {self.__numero_sala} {self.__telefono}{self.__correo}"