from conexion_parcial_1 import Conexion_parcial_1

class Pelicula(Conexion_parcial_1):
    def __init__(self,id_pelicula,titulo,director,duracion,genero,clasificacion,ano_produccion):

        self.__id_pelicula=id_pelicula
        self.__titulo=titulo
        self.__director=director
        self.__duracion=duracion
        self.__genero=genero
        self.__clasificacion=clasificacion
        self.__ano_produccion=ano_produccion
        super().__init__()
        self.__crear_Pelicula()

    def __crear_Pelicula(self):
        nom_clase = "pelicula"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_pelicula"):
            print("Tabla {} creada !!!".format(nom_clase))

    @property
    def _id_pelicula(self):
        return self.__id_pelicula

    @_id_pelicula.setter
    def _id_pelicula(self, value):
        self.__id_pelicula = value

    @property
    def _titulo(self):
        return self.__titulo

    @_titulo.setter
    def _titulo(self, value):
        self.__titulo = value

    @property
    def _director(self):
        return self.__director

    @_director.setter
    def _director(self, value):
        self.__director = value

    @property
    def _duracion(self):
        return self.__duracion

    @_duracion.setter
    def _duracion(self, value):
        self.__duracion = value

    @property
    def _genero(self):
        return self.__genero

    @_genero.setter
    def _genero(self, value):
        self.__genero = value

    @property
    def _clasificacion(self):
        return self.__clasificacion

    @_clasificacion.setter
    def _clasificacion(self, value):
        self.__clasificacion = value

    @property
    def _ano_produccion(self):
        return self.__ano_produccion

    @_ano_produccion.setter
    def _ano_produccion(self, value):
        self.__ano_produccion = value

    def id_pelicula(self):
        return self.__id_pelicula

    @id_pelicula.setter
    def id_pelicula(self, value):
        self.__id_pelicula = value

    @property
    def titulo(self):
        return self.__titulo

    @titulo.setter
    def titulo(self, value):
        self.__titulo = value

    @property
    def director(self):
        return self.__director

    @director.setter
    def director(self, value):
        self.__director = value

    @property
    def duracion(self):
        return self.__duracion

    @duracion.setter
    def duracion(self, value):
        self.__duracion = value

    @property
    def genero(self):
        return self.__genero

    @genero.setter
    def genero(self, value):
        self.__genero = value

    @property
    def clasificacion(self):
        return self.__clasificacion

    @clasificacion.setter
    def clasificacion(self, value):
        self.__clasificacion = value

    @property
    def ano_produccion(self):
        return self.__ano_produccion

    @ano_produccion.setter
    def ano_produccion(self, value):
        self.__ano_produccion = value
        
            
    
    def create_pelicula(self):
        
        self.insert_data(
            table_name="pelicula",
            data={
                "id_pelicula": self.id_pelicula,
                "titulo": self.titulo,
                "director": self.director,
                "duracion": self.duracion,
                "genero": self.genero,
                "clasificacion": self.clasificacion,
                "ano_produccion": self.ano_produccion
            }
        )

    @staticmethod
    def read_pelicula(id_pelicula):
        
        pelicula_data = Conexion_parcial_1.get_data(
            table_name="pelicula",
            where_conditions={"id_pelicula": id_pelicula}
        )

        if pelicula_data:
            data_dict = pelicula_data[0]  
            return Pelicula(
                id_pelicula=data_dict["id_pelicula"],
                titulo=data_dict["titulo"],
                director=data_dict["director"],
                duracion=data_dict["duracion"],
                genero=data_dict["genero"],
                clasificacion=data_dict["clasificacion"],
                ano_produccion=data_dict["ano_produccion"]
            )
        else:
            return None

    def update_pelicula(self):
        
        self.update_data(
            table_name="pelicula",
            data={
                "titulo": self.titulo,
                "director": self.director,
                "duracion": self.duracion,
                "genero": self.genero,
                "clasificacion": self.clasificacion,
                "ano_produccion": self.ano_produccion
            },
            where_conditions={"id_pelicula": self.id_pelicula}
        )

    def delete_pelicula(self):
       
        self.delete_data(
            table_name="pelicula",
            where_conditions={"id_pelicula": self.id_pelicula}
        )

    def __str__(self):
     return f"{self.__id_pelicula}{self.__titulo} {self.__director} {self.__duracion} {self.__genero} {self.__clasificacion}{self.__ano_produccion  }"