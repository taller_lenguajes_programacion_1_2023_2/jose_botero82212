from conexion_parcial_1 import Conexion_parcial_1

class Espectador(Conexion_parcial_1):
    def __init__(self,dni,nombre,apellido,fecha_nacimiento,telefono,correo):
        self.__dni=dni
        self.__nombre=nombre
        self.__apellido=apellido
        self.__fecha_nacimiento=fecha_nacimiento
        self.__telefono=telefono
        self.__correo=correo
        super().__init__()
        self.__crear_Espectador()

    def __crear_Espectador(self):
        nom_clase = "espectador"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_espectador"):
            print("Tabla {} creada !!!".format(nom_clase))

    @property
    def _dni(self):
        return self.__dni

    @_dni.setter
    def _dni(self, value):
        self.__dni = value

    @property
    def _nombre(self):
        return self.__nombre

    @_nombre.setter
    def _nombre(self, value):
        self.__nombre = value

    @property
    def _apellido(self):
        return self.__apellido

    @_apellido.setter
    def _apellido(self, value):
        self.__apellido = value

    @property
    def _fecha_nacimiento(self):
        return self.__fecha_nacimiento

    @_fecha_nacimiento.setter
    def _fecha_nacimiento(self, value):
        self.__fecha_nacimiento = value

    @property
    def _telefono(self):
        return self.__telefono

    @_telefono.setter
    def _telefono(self, value):
        self.__telefono = value

    @property
    def _correo(self):
        return self.__correo

    @_correo.setter
    def _correo(self, value):
        self.__correo = value
    
    def create_espectador(self):
        
        self.insert_data(
            table_name="espectador",
            data={
                "dni": self.dni,
                "nombre": self.nombre,
                "apellido": self.apellido,
                "fecha_nacimiento": self.fecha_nacimiento,
                "telefono": self.telefono,
                "correo": self.correo
            }
        )


    @staticmethod
    def read_espectador(dni):
      
        espectador_data = Conexion_parcial_1.get_data(
            table_name="espectador",
            where_conditions={"dni": dni}
        )

        if espectador_data:
            data_dict = espectador_data[0]  
            return Espectador(
                dni=data_dict["dni"],
                nombre=data_dict["nombre"],
                apellido=data_dict["apellido"],
                fecha_nacimiento=data_dict["fecha_nacimiento"],
                telefono=data_dict["telefono"],
                correo=data_dict["correo"]
            )
        else:
            return None

    def update_espectador(self):
       
        self.update_data(
            table_name="espectador",
            data={
                "nombre": self.nombre,
                "apellido": self.apellido,
                "fecha_nacimiento": self.fecha_nacimiento,
                "telefono": self.telefono,
                "correo": self.correo
            },
            where_conditions={"dni": self.dni}
        )

    def delete_espectador(self):
        
        self.delete_data(
            table_name="espectador",
            where_conditions={"dni": self.dni}
        )
        
    def __str__(self):
     return f"{self.__dni}{self.__nombre} {self.__apellido} {self.__fecha_nacimiento} {self.__telefono} {self.__correo}"    