from cine import Cine
from excel_parcial import Excel_parcial
from espectador import Espectador
from pelicula import Pelicula
from conexion_parcial_1 import Conexion_parcial_1
from excel_parcial import Excel_parcial


class Proyeccion(Conexion_parcial_1):
    def __init__(self,id_proyeccion,sala,horario,precio):
        
        self.__xlsx = Excel_parcial("excel_parcial.xlsx")
        self.__df_proyec = self.__xlsx.leer_xlsx("vehiculos")
        self.__lista_proyec=self.__df_a_proyec()
        self.__id_proyeccion=id_proyeccion
        self.__sala=sala
        self.__horario=horario
        self.__cine=Cine()
        self.__espectador=Espectador()
        self.__pelicula=Pelicula()
        self.__precio=precio
        super().__init__()
        self.__crear_Proyeccion()

    def __crear_Proyeccion(self):
        nom_clase = "proyeccion"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_proyeccion"):
            print("Tabla {} creada !!!".format(nom_clase))

    def obtener_id(self,num=-1):
        if num < 0:
            return self.__lista_proyec["numero"]
        else:
            self.__id_proyeccion= self.__lista_proyec["numero"][num]
        return self.__id_proyeccion
        
        
    @property
    def _id_proyeccion(self):
        return self.__id_proyeccion

    @_id_proyeccion.setter
    def _id_proyeccion(self, value):
        self.__id_proyeccion = value

    @property
    def _sala(self):
        return self.__sala

    @_sala.setter
    def _sala(self, value):
        self.__sala = value

    @property
    def _horario(self):
        return self.__horario

    @_horario.setter
    def _horario(self, value):
        self.__horario = value

    @property
    def _cine(self):
        return self.__cine

    @_cine.setter
    def _cine(self, value):
        self.__cine = value

    @property
    def _espectador(self):
        return self.__espectador

    @_espectador.setter
    def _espectador(self, value):
        self.__espectador = value

    @property
    def _pelicula(self):
        return self.__pelicula

    @_pelicula.setter
    def _pelicula(self, value):
        self.__pelicula = value

    @property
    def _precio(self):
        return self.__precio

    @_precio.setter
    def _precio(self, value):
        self.__precio = value

    @property
    def id_proyeccion(self):
        return self.__id_proyeccion

    @id_proyeccion.setter
    def id_proyeccion(self, value):
        self.__id_proyeccion = value

    @property
    def sala(self):
        return self.__sala

    @sala.setter
    def sala(self, value):
        self.__sala = value

    @property
    def horario(self):
        return self.__horario

    @horario.setter
    def horario(self, value):
        self.__horario = value

    @property
    def cine(self):
        return self.__cine

    @cine.setter
    def cine(self, value):
        self.__cine = value

    @property
    def espectador(self):
        return self.__espectador

    @espectador.setter
    def espectador(self, value):
        self.__espectador = value

    @property
    def pelicula(self):
        return self.__pelicula

    @pelicula.setter
    def pelicula(self, value):
        self.__pelicula = value

    @property
    def precio(self):
        return self.__precio

    @precio.setter
    def precio(self, value):
        self.__precio = value

    def __df_a_proyec(self):
        df = self.__df_proyec
        lista_proyec = df.to_dict()
        return lista_proyec    
    
    def __df_a_proyec(self):
        df = self.__df_proyec
        lista_proyec = df.to_dict()
        return lista_proyec
    
    @staticmethod
    def read_proyeccion(id_proyeccion):
        
        proyeccion_data = Conexion_parcial_1.get_data(
            table_name="proyeccion",
            where_conditions={"id_proyeccion": id_proyeccion}
        )

        if proyeccion_data:
            data_dict = proyeccion_data[0]  
            return Proyeccion(
                id_proyeccion=data_dict["id_proyeccion"],
                sala=data_dict["sala"],
                horario=data_dict["horario"],
                precio=data_dict["precio"]
            )
        else:
            return None


    def update_proyeccion(self):
        
        self.update_data(
            table_name="proyeccion",
            data={
                "sala": self.sala,
                "horario": self.horario,
                "cine_id": self.cine.id_cine,  
                "espectador_dni": self.espectador.dni,
                "pelicula_id": self.pelicula.id_pelicula, 
                "precio": self.precio
            },
            where_conditions={"id_proyeccion": self.id_proyeccion}
        )

    def delete_proyeccion(self):
        
        self.delete_data(
            table_name="proyeccion",
            where_conditions={"id_proyeccion": self.id_proyeccion}
        )

    def __str__(self):
     return f"{self.__id_proyeccion}{self.__sala} {self.__horario} {self.__cine} {self.__espectador}{self.__pelicula} {self.__precio}"
       
        
  