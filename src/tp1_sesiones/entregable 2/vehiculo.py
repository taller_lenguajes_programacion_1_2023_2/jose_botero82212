from coche import Coche
from excel_entregable import Excel_entregable
from conexion_entregable import Conexion_entregable

class Vehiculo(Conexion_entregable):
    def __init__(self,id = 0,marca="",modelo="",ano_fabricacion="",combustible="",km=0):
        
        self.__xlsx = Excel_entregable("entregable_2.xlsx")
        self.__df_veh = self.__xlsx.leer_xlsx("vehiculos")
        self.__lista_veh=self.__df_a_vehiculos()
        self.__id = id
        self.__marca=marca
        self.__modelo=modelo
        self.__ano_fabricacion=ano_fabricacion
        self.__combustible=combustible
        self.__km=km
        self.__coche = Coche()

        super().__init__()
        self.__crear_Vehiculo()
        self.__insertar_datos()
         
    def __crear_Vehiculo(self):
        nom_clase = "vehiculos"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_vehiculo"):
            print("Tabla {} creada !!!".format(nom_clase))
    
    
    def obtener_id(self,num=-1):
        if num < 0:
            return self.__lista_veh["numero"]
        else:
            self.__id= self.__lista_veh["numero"][num]
        return self.__id
      
    def __insertar_datos(self):
        df = self.__df_veh
        nom_colunas = "colums_veh"
        for idx, row in df.iterrows():
            valores = '{}, "{}" , "{}" , "{}" , "{}" , "{}" , {} '.format(row["id"],row["marca"],row["modelo"],row["ano_fabricacion"],row["combustible"],row["km"],row["coche"])
            if self.insert_datos(nom_tabla="vehiculos",nom_columnas=nom_colunas,valores=valores):
                print("SI inserto en la de vehiculos : ",valores)
            else:
                print("NO INSERTO en la de vehiculos : ",valores)
                

    def crear_datos(self):
       
        marca = input("Marca: ")
        modelo = input("Modelo: ")
        ano_fabricacion = input("Año de fabricación: ")
        combustible = input("Combustible: ")
        km = input("Kilómetros: ")

       
        new_entry = {
            "id": self.obtener_id(),  
            "marca": marca,
            "modelo": modelo,
            "ano_fabricacion": ano_fabricacion,
            "combustible": combustible,
            "km": km,
            "coche": self._coche  
        }

      
        self.__df_veh = self.__df_veh.append(new_entry, ignore_index=True)
        self.__xlsx.guardar_xlsx("vehiculos", self.__df_veh)
        print("Datos creados con éxito!")

    def update_datos(self, id_to_update):
      
        if id_to_update not in self.__lista_veh["numero"]:
            print(f"ID {id_to_update} no encontrado en la lista de vehículos.")
            return

        
        marca = input("Nueva marca: ")
        modelo = input("Nuevo modelo: ")
        ano_fabricacion = input("Nuevo año de fabricación: ")
        combustible = input("Nuevo combustible: ")
        km = input("Nuevos kilómetros: ")

       
        idx = self.__lista_veh["numero"].index(id_to_update)
        self.__df_veh.loc[idx, "marca"] = marca
        self.__df_veh.loc[idx, "modelo"] = modelo
        self.__df_veh.loc[idx, "ano_fabricacion"] = ano_fabricacion
        self.__df_veh.loc[idx, "combustible"] = combustible
        self.__df_veh.loc[idx, "km"] = km

       
        self.__xlsx.guardar_xlsx("vehiculos", self.__df_veh)
        print(f"Datos actualizados con éxito para el ID {id_to_update}.")

    def del_datos(self, id_to_delete):
       
        if id_to_delete not in self.__lista_veh["numero"]:
            print(f"ID {id_to_delete} no encontrado en la lista de vehículos.")
            return

        
        idx = self.__lista_veh["numero"].index(id_to_delete)
        self.__df_veh.drop(index=idx, inplace=True)

       
        self.__xlsx.guardar_xlsx("vehiculos", self.__df_veh)
        print(f"Datos borrados con éxito para el ID {id_to_delete}.")
    
    @property
    def _lista_veh(self):
        return self.__lista_veh

    @_lista_veh.setter
    def _lista_veh(self, value):
        self.__lista_veh = value

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _marca(self):
        return self.__marca

    @_marca.setter
    def _marca(self, value):
        self.__marca = value

    @property
    def _modelo(self):
        return self.__modelo

    @_modelo.setter
    def _modelo(self, value):
        self.__modelo = value

    @property
    def _ano_fabricacion(self):
        return self.__ano_fabricacion

    @_ano_fabricacion.setter
    def _ano_fabricacion(self, value):
        self.__ano_fabricacion = value

    @property
    def _combustible(self):
        return self.__combustible

    @_combustible.setter
    def _combustible(self, value):
        self.__combustible = value

    @property
    def _km(self):
        return self.__km

    @_km.setter
    def _km(self, value):
        self.__km = value

    @property
    def _coche(self):
        return self.__coche

    @_coche.setter
    def _coche(self, value):
        self.__coche = value

    def __df_a_vehiculos(self):
        df = self.__df_veh
        lista_veh = df.to_dict()
        return lista_veh
        
    def __str__(self):
        return f"{self.__id} {self.__marca} {self.__modelo} {self.__ano_fabricacion} {self.__combustible}{self.__km}{self.__coche}"
