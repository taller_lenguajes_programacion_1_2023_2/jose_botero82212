from conexion_entregable import Conexion_entregable

class Coche(Conexion_entregable):
    def __init__(self,id = 0,marca="",modelo="",ano_fabricacion="",combustible="",km=0,color="",num_puertas=0):

        self.__id = id
        self.__marca=marca
        self.__modelo=modelo
        self.__ano_fabricacion=ano_fabricacion
        self.__combustible=combustible
        self.__km=km
        self.__color=color
        self.__num_puertas=num_puertas
        super().__init__()
        self.__crear_Coche()
    
    def __crear_Coche(self):
        nom_clase = "coches"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_coche"):
            print("Tabla {} creada !!!".format(nom_clase))

    def actualizar_datos(self):
        
        id_to_update = int(input("Ingrese el ID del coche que desea actualizar: "))

       
        if id_to_update not in self.__lista_coche["id"]:
            print(f"ID {id_to_update} no encontrado en la lista de coches.")
            return

        
        marca = input("Nueva marca: ")
        modelo = input("Nuevo modelo: ")
        ano_fabricacion = input("Nuevo año de fabricación: ")
        combustible = input("Nuevo combustible: ")
        km = input("Nuevos kilómetros: ")
        color = input("Nuevo color: ")
        num_puertas = input("Nuevo número de puertas: ")

      
        idx = self.__lista_coche["id"].index(id_to_update)
        self.__df_coche.loc[idx, "marca"] = marca
        self.__df_coche.loc[idx, "modelo"] = modelo
        self.__df_coche.loc[idx, "ano_fabricacion"] = ano_fabricacion
        self.__df_coche.loc[idx, "combustible"] = combustible
        self.__df_coche.loc[idx, "km"] = km
        self.__df_coche.loc[idx, "color"] = color
        self.__df_coche.loc[idx, "num_puertas"] = num_puertas

        
        self.__xlsx.guardar_xlsx("coches", self.__df_coche)
        print(f"Datos actualizados con éxito para el ID {id_to_update}.")

    def eliminar_datos(self):
       
        id_to_delete = int(input("Ingrese el ID del coche que desea eliminar: "))

       
        if id_to_delete not in self.__lista_coche["id"]:
            print(f"ID {id_to_delete} no encontrado en la lista de coches.")
            return

        
        idx = self.__lista_coche["id"].index(id_to_delete)
        self.__df_coche.drop(index=idx, inplace=True)

        
        self.__xlsx.guardar_xlsx("coches", self.__df_coche)
        print(f"Datos eliminados con éxito para el ID {id_to_delete}.")


        coche_instance = Coche()
        coche_instance.actualizar_datos()
        coche_instance.eliminar_datos()


    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _marca(self):
        return self.__marca

    @_marca.setter
    def _marca(self, value):
        self.__marca = value

    @property
    def _modelo(self):
        return self.__modelo

    @_modelo.setter
    def _modelo(self, value):
        self.__modelo = value

    @property
    def _ano_fabricacion(self):
        return self.__ano_fabricacion

    @_ano_fabricacion.setter
    def _ano_fabricacion(self, value):
        self.__ano_fabricacion = value

    @property
    def _combustible(self):
        return self.__combustible

    @_combustible.setter
    def _combustible(self, value):
        self.__combustible = value

    @property
    def _km(self):
        return self.__km

    @_km.setter
    def _km(self, value):
        self.__km = value

    @property
    def _color(self):
        return self.__color

    @_color.setter
    def _color(self, value):
        self.__color = value

    @property
    def _num_puertas(self):
        return self.__num_puertas

    @_num_puertas.setter
    def _num_puertas(self, value):
        self.__num_puertas = value

        
    def __str__(self) :
        return f"Coches {self.__id} {self.__marca}{self.__modelo}{self.__ano_fabricacion} {self.__combustible}{self.__km}{self.__color}{self._num_puertas}"
