from django.db import models

class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(auto_now_add=True)
    f_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Usuario(AuditoriaFecha):
    usuario = models.CharField(max_length=255)
    clave = models.CharField(max_length=255)

class Persona(AuditoriaFecha):
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    f_nacimiento = models.DateField()
    usuario = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)

class UsuarioRegistrado(models.Model):
    imagen = models.ImageField(upload_to='user_images/', blank=True, null=True)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    cedula = models.CharField(max_length=20)
    correo = models.EmailField()
    usuario = models.CharField(max_length=50)
    contraseña = models.CharField(max_length=100)
    habilidades = models.TextField()
    herramientas = models.TextField()
