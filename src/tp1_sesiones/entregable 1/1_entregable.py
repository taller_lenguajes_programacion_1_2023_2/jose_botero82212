## 3. dada una variable s con el valor "12345", escribe un programa para convertir en un numero entero

jmbs_s=12345

jmbs_numero_entero=int(jmbs_s)

## 13. Escribe un programa que tome una cadena s y la convierta en mayuscula

jmbs_s = input("Ingrese una cadena: ")

jmbs_cadena_mayusculas = jmbs_s.upper()

print("Cadena en mayúsculas:", jmbs_cadena_mayusculas)

## 23. Dada una lista de numeros, escribe un programa que devuelva una lista con solo los numeros pares.

jmbs_numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

jmbs_numeros_pares = [jmbs_numero for jmbs_numero in jmbs_numeros if jmbs_numero % 2 == 0]

print("Números pares:", jmbs_numeros_pares)

## 33. Escribe un programa que determine si una lista es una sublista de otra lista.

def jmbs_sublista(lista, sublista):
    if len(sublista) == 0:
        return True 
    for i in range(len(lista)):
        if lista[i:i+len(sublista)] == sublista:
            return True
    return False

lista_principal = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
sublista = [3, 4, 5]

if jmbs_sublista(lista_principal, sublista):
    print("La sublista es parte de la lista principal.")
else:
    print("La sublista no es parte de la lista principal.")

## 43. Escribe un programa que lea un archivo csv y lo exporte a un archivo excel

import pandas as pd

jmbs_data = pd.read_csv('./src/tp1_sesiones/static/csv/entregable1.csv')

jmbs_archivo_excel = 'documento.xlsx'

writer = pd.ExcelWriter(jmbs_archivo_excel, engine='xlsxwriter')

jmbs_data.to_excel(writer, sheet_name='Hoja1', index=False)

writer.save()

print(f'Se ha exportado el archivo CSV a {jmbs_archivo_excel}')

## 53. Simula un archivo csv, escribe un programa que muestre las filas que contienen valores faltantes o nulos

import pandas as pd
import numpy as np

jmbs_datos = {
    'Nombre': ['Juan', 'María', 'Carlos', 'Luis', None],
    'Edad': [25, None, 30, 22, 28],
    'Ciudad': ['Medellin', 'Bogota', None, 'Rionegro', 'Cartagena']
}

df = pd.DataFrame(jmbs_datos)

jmbs_valores_faltantes = df[df.isnull().any(axis=1)]

print("DataFrame original:")
print(df)
print("\nFilas con valores faltantes o nulos:")
print(jmbs_valores_faltantes)

## 63. Escribe un programa que lea un archivo html desde la pagina que posea una tabla y muestre sus primeros 10 registros

import pandas as pd

jmbs_url = "https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)_per_capita"
list_html = pd.read_html(jmbs_url)

df = list_html[1]  

jmbs_10_registros = df.head(10)
print(jmbs_10_registros)

## 73. Escribe un programa que lea  un archivo excel y remplace un valor especifico en una columna determinada.

import pandas as pd


jmbs_excel = 'src/tp1_sesiones/static/xlsx/entregable1.xlsx'
df = pd.read_excel(jmbs_excel)

columna_objetivo = 'Profesión'
valor_a_buscar = 'Diseñador' 

df[columna_objetivo] = df[columna_objetivo].replace(valor_a_buscar, 'Arqueologo')

df.to_excel('documento_entregable1.xlsx', index=False)  

print('Reemplazo completado y archivo guardado.')

## 83. Escribe un programa que lea un archivo html y transforme la fecha de una columna a formato 'año-mes-dia'

import pandas as pd

url = "https://publicholidays.co/es/2023-dates/"
df = pd.read_html(url)[1] 

def transformar_fecha(fecha):
    
    match = re.match(r'(\d{4})/(\d{2})/(\d{2})', fecha)
    if match:
        return f"{match.group(1)}-{match.group(2)}-{match.group(3)}"
    else:
        return fecha

df['Fecha'] = df['Fecha'].apply(transformar_fecha)

print(df)

## 93. Escribe un programa que lea un archivo csv y utilice la funcion crosstab para crear una tabla de contigencia

import pandas as pd

jmbs_archivo_csv = './src/tp1_sesiones/static/csv/entregable1.csv' 
df = pd.read_csv(jmbs_archivo_csv)

jmbs_tabla_contingencia = pd.crosstab(df['Marca'], df['Modelo'])

print("Tabla de contingencia:")
print(jmbs_tabla_contingencia)