import tkinter as tk 
from tkinter import ttk
from model.aerolinea import Aerolinea
from vuelo_app import VueloFrame


class Frame(tk.Frame):
    def __init__(self,root=None):
        super().__init__(root,width=480,height=320)
        self.root = root
        self.pack()
        self.id_aerolinea = None
        self.aerolinea = Aerolinea()
        self.campos_aerolinea()
        self.desabilitar_campos()
        self.tabla_aerolinea()
        
    def campos_aerolinea(self):
        self.lbl_nombre = tk.Label(self,text='Nombre: ')
        self.lbl_nombre.config(font=('Arial',12,'bold'))
        self.lbl_nombre.grid(row=0,column=0,padx=10,pady=10)
        
        self.lbl_hub = tk.Label(self,text='Centro de Operaciones: ')
        self.lbl_hub.config(font=('Arial',12,'bold'))
        self.lbl_hub.grid(row=1,column=0,padx=10,pady=10)
         
        self.var_nombre = tk.StringVar()
        self.input_nombre = tk.Entry(self,textvariable=self.var_nombre)
        self.input_nombre.config(width=50,font=('Arial',12))
        self.input_nombre.grid(row=0,column=1,padx=10,pady=10,columnspan=2)
        
        self.var_hub = tk.StringVar()
        self.input_hub = tk.Entry(self,textvariable=self.var_hub)
        self.input_hub.config(width=50,font=('Arial',12))
        self.input_hub.grid(row=1,column=1,padx=10,pady=10,columnspan=2)
    
        self.btn_nuevo = tk.Button(self,text='Nuevo',command=self.habilitar_campos)
        self.btn_nuevo.config(width=20, font=('Arial',12,'bold'))
        self.btn_nuevo.grid(row=3,column=0,padx=10,pady=10)
        
        self.btn_guardar = tk.Button(self,text='Guardar',command=self.guardar_datos)
        self.btn_guardar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_guardar.grid(row=3,column=1,padx=10,pady=10)
        
        self.btn_cancelar = tk.Button(self,text='Cancelar',command=self.desabilitar_campos)
        self.btn_cancelar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_cancelar.grid(row=3,column=2,padx=10,pady=10)
        
    def habilitar_campos(self):
        self.var_nombre.set('')
        self.var_hub.set('')

        self.input_nombre.config(state='normal')
        self.input_hub.config(state='normal')
        
        self.btn_guardar.config(state='normal')
        self.btn_cancelar.config(state='normal')
        
    
    def desabilitar_campos(self):
        self.var_nombre.set('')
        self.var_hub.set('')
      
        self.input_nombre.config(state='disabled')
        self.input_hub.config(state='disabled')
      
        self.btn_guardar.config(state='disabled')
        self.btn_cancelar.config(state='disabled')
    
    def guardar_datos(self):
        
        self.aerolinea = Aerolinea(
            self.var_nombre.get(),
            self.var_hub.get()
        )
        if self.id_aerolinea == None:
           self.aerolinea.guardar() 
        else:
           self.aerolinea.editar(self.id_aerolinea) 
        self.tabla_aerolinea()
        self.desabilitar_campos()
    
    def tabla_aerolinea(self):
        self.lista_aerolinea = self.aerolinea.listar()
        self.lista_aerolinea.reverse()
        
        self.tabla = ttk.Treeview(self,
        columns=('Nombre', 'Centro Operaciones'))
        self.tabla.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.scroll = ttk.Scrollbar(self,orient='vertical',command=self.tabla.yview)
        self.scroll.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.tabla.configure(yscrollcommand=self.scroll.set)
        self.tabla.heading('#0',text='ID')
        self.tabla.heading('#1',text='NOMBRE')
        self.tabla.heading('#2',text='centro de operaciones')
        for aero in self.lista_aerolinea:
          if len(aero) >= 4: 
           self.tabla.insert('', 0, text=aero[0], values=(aero[1], aero[2], aero[3]))

        self.boton_editar = tk.Button(self,text="Editar",command=self.editar_datos)
        self.boton_editar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_editar.grid(row=5,column=0,padx=10,pady=10)
        
        self.boton_eliminar = tk.Button(self,text="Eliminar",command=self.eliminar_datos)
        self.boton_eliminar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_eliminar.grid(row=5,column=1,padx=10,pady=10)

        self.boton_nueva_ventana = tk.Button(self, text="Reservar Vuelo", command=self.mostrar_vuelo)
        self.boton_nueva_ventana.config(width=20, font=('Arial', 12, 'bold'))
        self.boton_nueva_ventana.grid(row=5, column=2, padx=10, pady=10)

    def mostrar_vuelo(self):
        root_vuelo = tk.Toplevel(self.root)
        root_vuelo.title('Vuelo')
        app_vuelo = VueloFrame(root=root_vuelo)
        app_vuelo.mainloop()

    def editar_datos(self):
        try:
            self.id_aerolinea = self.tabla.item(self.tabla.selection())['text']
            self.nom_aerolinea = self.tabla.item(self.tabla.selection())['values'][0]
            self.hub_aerolinea= self.tabla.item(self.tabla.selection())['values'][1]
            self.habilitar_campos()
            self.input_nombre.insert(0,self.nom_aerolinea)
            self.input_hub.insert(0,self.hub_aerolinea)
            
        except Exception as error:
            print(error)
    
    def eliminar_datos(self):
        try:
            self.id_pelicula = self.tabla.item(self.tabla.selection())['text']
            self.aerolinea.eliminar(self.id_pelicula)
            self.tabla_aerolinea()
            self.id_pelicula=None
            
        except Exception as error:
            print(error)   
