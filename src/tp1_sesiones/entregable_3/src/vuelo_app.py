import tkinter as tk
from tkinter import ttk
from model.vuelo import Vuelo

class VueloFrame(tk.Frame):
    def __init__(self, root=None):
        super().__init__(root, width=480, height=320)
        self.root = root
        self.pack()
        self.id_vuelo = None
        self.vuelo = Vuelo()
        self.campos_vuelo()
        self.desabilitar_campos()
        self.tabla_vuelo()

    def campos_vuelo(self):
        self.lbl_numero_vuelo = tk.Label(self,text='Numero vuelo: ')
        self.lbl_numero_vuelo.config(font=('Arial',12,'bold'))
        self.lbl_numero_vuelo.grid(row=0,column=0,padx=10,pady=10)
        
        self.lbl_id_aerolinea = tk.Label(self,text='Id aerolinea: ')
        self.lbl_id_aerolinea.config(font=('Arial',12,'bold'))
        self.lbl_id_aerolinea.grid(row=1,column=0,padx=10,pady=10)
         
        self.var_numero_vuelo = tk.StringVar()
        self.input_numero_vuelo = tk.Entry(self,textvariable=self.var_numero_vuelo)
        self.input_numero_vuelo.config(width=50,font=('Arial',12))
        self.input_numero_vuelo.grid(row=0,column=1,padx=10,pady=10,columnspan=2)
        
        self.var_id_aerolinea = tk.StringVar()
        self.input_id_aerolinea = tk.Entry(self,textvariable=self.var_id_aerolinea)
        self.input_id_aerolinea.config(width=50,font=('Arial',12))
        self.input_id_aerolinea.grid(row=1,column=1,padx=10,pady=10,columnspan=2)
    
        self.btn_nuevo = tk.Button(self,text='Nuevo',command=self.habilitar_campos)
        self.btn_nuevo.config(width=20, font=('Arial',12,'bold'))
        self.btn_nuevo.grid(row=3,column=0,padx=10,pady=10)
        
        self.btn_guardar = tk.Button(self,text='Guardar',command=self.guardar_datos)
        self.btn_guardar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_guardar.grid(row=3,column=1,padx=10,pady=10)
        
        self.btn_cancelar = tk.Button(self,text='Cancelar',command=self.desabilitar_campos)
        self.btn_cancelar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_cancelar.grid(row=3,column=2,padx=10,pady=10)
        
    def habilitar_campos(self):
        self.var_numero_vuelo.set('')
        self.var_id_aerolinea.set('')

        self.input_numero_vuelo.config(state='normal')
        self.input_id_aerolinea.config(state='normal')
        
        self.btn_guardar.config(state='normal')
        self.btn_cancelar.config(state='normal')
        
    
    def desabilitar_campos(self):
        self.var_numero_vuelo.set('')
        self.var_id_aerolinea.set('')
      
        self.input_numero_vuelo.config(state='disabled')
        self.input_id_aerolinea.config(state='disabled')
      
        self.btn_guardar.config(state='disabled')
        self.btn_cancelar.config(state='disabled')
    
    def guardar_datos(self):
        
        self.vuelo = Vuelo(
            self.var_numero_vuelo.get(),
            self.var_id_aerolinea.get()
        )
        if self.id_vuelo == None:
           self.vuelo.guardar() 
        else:
           self.vuelo.editar(self.id_vuelo) 
        self.tabla_vuelo()
        self.desabilitar_campos()

    def tabla_vuelo(self):
        self.lista_vuelo = self.vuelo.listar()
        self.lista_vuelo.reverse()
        
        self.tabla = ttk.Treeview(self,
        columns=('Numero', 'Id aerolinea'))
        self.tabla.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.scroll = ttk.Scrollbar(self,orient='vertical',command=self.tabla.yview)
        self.scroll.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.tabla.configure(yscrollcommand=self.scroll.set)
        self.tabla.heading('#0',text='ID vuelo')
        self.tabla.heading('#1',text='Numero')
        self.tabla.heading('#2',text='Id aerolinea')
        for aero in self.lista_vuelo:
          if len(aero) >= 4: 
           self.tabla.insert('', 0, text=aero[0], values=(aero[1], aero[2], aero[3]))

        self.boton_editar = tk.Button(self,text="Editar",command=self.editar_datos)
        self.boton_editar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_editar.grid(row=5,column=0,padx=10,pady=10)
        
        self.boton_eliminar = tk.Button(self,text="Eliminar",command=self.eliminar_datos)
        self.boton_eliminar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_eliminar.grid(row=5,column=1,padx=10,pady=10)

    def editar_datos(self):
        try:
            self.id_vuelo = self.tabla.item(self.tabla.selection())['text']
            self.numero_vuelo = self.tabla.item(self.tabla.selection())['values'][0]
            self.id_aerolinea = self.tabla.item(self.tabla.selection())['values'][1]
            self.habilitar_campos()
            self.input_id_aerolinea.insert(0, self.id_vuelo)
        except Exception as error:
            print(error)

    def eliminar_datos(self):
        try:
            self.id_vuelo = self.tabla.item(self.tabla.selection())['text']
            self.vuelo.eliminar(self.id_vuelo)
            self.tabla_vuelo()
            self.id_vuelo = None
        except Exception as error:
            print(error)

if __name__ == "__main__":
    try:
        root = tk.Tk()
        root.title('Vuelo')
        app = VueloFrame(root)
        app.mainloop()
    except Exception as e:
        print(f"Error: {e}")
