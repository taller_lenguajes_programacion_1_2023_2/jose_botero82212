import sqlite3
from tkinter import messagebox
class Conexion_Aerolinea:
    def __init__(self):
        self.ruta_db = 'D:/Taller de programacion/Repositorio/jose_botero82212/src/tp1_sesiones/entregable_3/src/static/db/aerolinea.db'
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        self.crear_tabla()
    
    def crear_tabla(self):
        query = """
        CREATE TABLE aerolinea (
        id_aerolinea INTEGER PRIMARY KEY,
        nombre VARCHAR (100),
        hub VARCHAR (10)
        )"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Creación Tabla Exitosa"," Se creo la tabla correctamente ")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Creación Tabla No Exitosa"," No se pudo crear la tabla")

    def borrar_tabla(self):
        query= """
        DROP TABLE aerolinea"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Borrado Tabla Exitosa"," Se elimino la tabla correctamente. ")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Borrado Tabla No Exitosa"," No se pudo elimnar la tabla.")
   
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()