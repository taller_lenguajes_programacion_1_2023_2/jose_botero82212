from .conexion_vuelo import Conexion_Vuelo
from tkinter import messagebox
class Vuelo(Conexion_Vuelo):
    def __init__(self, numero_vuelo="", id_aerolinea=""):
        super().__init()
        self.id_vuelo = None
        self.numero_vuelo = numero_vuelo
        self.id_aerolinea = None
        self.conexion = Conexion_Vuelo()
        self.cursor = self.conexion.cursor

    def guardar(self):
        query = """
        INSERT INTO vuelo (numero_vuelo, id_aerolinea)
        VALUES ('{}', '{}')
        """.format(self.numero_vuelo, self.id_aerolinea)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
            messagebox.showinfo("INFO: Guardado Exitoso", "Se guardó correctamente el registro")
        except Exception as error:
            print(error)
            messagebox.showerror("ERROR: Guardado No Exitoso", "No se pudo guardar el registro")

    def editar(self, id_vuelo=""):
        if id_vuelo:
            query = """
            UPDATE vuelo
            SET numero_vuelo = '{}', id_aerolinea = '{}'
            WHERE id_vuelo = {}
            """.format(self.numero_vuelo, self.id_aerolinea, id_vuelo)

            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo("INFO: Edición Exitosa", "Se editó correctamente el registro")
            except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Edición No Exitosa", "No se pudo editar el registro")
        else:
            print("Error: no hay id_vuelo para editar")

    def listar(self):
        self.conexion = Conexion_Vuelo()
        query = """
        SELECT * FROM vuelo
        """
        lista = []
        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        return lista

    def __str__(self) -> str:
        return "Vuelo {} , {}".format(self.numero_vuelo, self.id_aerolinea)
