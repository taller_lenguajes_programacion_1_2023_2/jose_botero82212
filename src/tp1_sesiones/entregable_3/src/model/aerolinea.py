from .conexion_aerolinea import Conexion_Aerolinea
from tkinter import messagebox

class Aerolinea(Conexion_Aerolinea):
    def __init__(self,nombre="",hub="") :
        super().__init__()
        self.id_pelicula = None
        self.nombre = nombre
        self.hub = hub
        self.conexion = Conexion_Aerolinea()
        self.cursor = self.conexion.cursor
        
    def guardar(self):
        query = """
        INSERT INTO aerolinea ( nombre, hub)
        VALUES ('{}' , '{}')
        """.format(self.nombre,self.hub)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def editar(self,id_aerolinea=""):
        query = """
        UPDATE aerolinea 
        SET nombre = '{}' , hub = '{}'
        WHERE id_aerolinea = {}
        """.format(self.nombre,self.hub,id_aerolinea)

        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def eliminar(self,id_aerolinea=""):
        if id_aerolinea != "":
            query = """
            DELETE FROM aerolinea WHERE id_aerolinea = '{}'
            """.format(id_aerolinea)
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo("INFO : Eliminacion Exitosa"," Se elimino correctamente el registro {}".format(id_aerolinea))
            except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Eliminacion No Exitosa"," No se pudo realizar la eliminacion")
        else:
            print("error: no hay id_aerolinea para eliminar")

    def listar(self):
        self.conexion = Conexion_Aerolinea()
        query = """
        SELECT * FROM aerolinea
        """
        lista= []
        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        return lista 

    def __str__(self) -> str:
        return "aerolinea {} , {} , {}".format(self.nombre,self.hub)