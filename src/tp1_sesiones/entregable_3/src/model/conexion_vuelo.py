import sqlite3
from tkinter import messagebox

class Conexion_Vuelo:
    def __init__(self, db_path):
        self.ruta_db = db_path
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()

        self.ruta_db = 'D:/Taller de programacion/Repositorio/jose_botero82212/src/tp1_sesiones/entregable_3/src/static/db/vuelo.db'
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        self.crear_tabla_vuelo()

    def crear_tabla_vuelo(self):
        query = """
        CREATE TABLE vuelo (
        id_vuelo INTEGER PRIMARY KEY,
        numero_vuelo VARCHAR(10),
        id_aerolinea INTEGER,
        FOREIGN KEY (id_aerolinea) REFERENCES aerolinea(id_aerolinea)
        )"""
        try:
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("INFO: Creación Tabla Vuelo Exitosa", "Se creó la tabla de vuelo correctamente")
        except Exception as error:
            print(error)
            messagebox.showerror("ERROR: Creación Tabla Vuelo No Exitosa", "No se pudo crear la tabla de vuelo")

    def borrar_tabla_vuelo(self):
        query = """
        DROP TABLE vuelo"""
        try:
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("INFO: Borrado Tabla Vuelo Exitosa", "Se eliminó la tabla de vuelo correctamente")
        except Exception as error:
            print(error)
            messagebox.showerror("ERROR: Borrado Tabla Vuelo No Exitosa", "No se pudo eliminar la tabla de vuelo")

    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
