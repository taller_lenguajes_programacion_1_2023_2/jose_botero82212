import pandas as pd
import re

url = "https://publicholidays.co/es/2023-dates/"
df = pd.read_html(url)[1] 

def transformar_fecha(fecha):
    
    match = re.match(r'(\d{4})/(\d{2})/(\d{2})', fecha)
    if match:
        return f"{match.group(1)}-{match.group(2)}-{match.group(3)}"
    else:
        return fecha

df['Fecha'] = df['Fecha'].apply(transformar_fecha)

print(df)
